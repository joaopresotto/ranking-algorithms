import os
import numpy as np
import load_data as load
import save_data as save
import img
import plots
import time
import operator
import statistics
import toolz
from math import ceil, floor
from copy import deepcopy
from operator import itemgetter, attrgetter, methodcaller
from scipy.spatial import distance


def create_ranked_lists_files(dataset):
    if dataset == "iris":
        filepath = os.path.join("Datasets", os.path.join("iris", "iris.data"))
        features, classes = load.load_iris_dataset(filepath=filepath)
    elif dataset == 'wine':
        filepath = os.path.join("Datasets", os.path.join("wine", "wine.data"))
        features, classes = load.load_wine_dataset(filepath=filepath)
    elif dataset == 'flowers':
        filepath = os.path.join("Datasets", os.path.join("flowers", "ACC")+".npz")
        features = np.load(filepath)["features"]
        classes = [i//50 for i in range(1360)]
    
    n_elems = features.shape[0]

    distance_matrix = np.ndarray(shape=(n_elems, n_elems), dtype=np.object)
    aux_matrix = np.ndarray(shape=(n_elems, n_elems), dtype=np.object)

    # Create distance matrix
    for i in range(n_elems):
        for j in range(n_elems):
            image = img.Image(
                index=j, distance=distance.euclidean(features[i], features[j]), classification=classes[j])
            distance_matrix[i, j] = image

    aux_matrix = deepcopy(distance_matrix)

    # Sorting distance matrix generating ranked lists of i in each line
    for i in range(n_elems):
        # O(n log n)
        aux_matrix[i] = sorted(aux_matrix[i], key=attrgetter('distance'))

    # Saving ranked lists in file
    ranked_lists_path = os.path.join("outputs", dataset)
    save.save_ranked_lists_file(
        ranked_lists_path, aux_matrix)

    return aux_matrix


def create_features_file(dataset, plot):
    if dataset == "iris":
        filepath = os.path.join("Datasets", os.path.join("iris", "iris.data"))
        features, classes = load.load_iris_dataset(filepath=filepath)
    elif dataset == 'wine':
        filepath = os.path.join("Datasets", os.path.join("wine", "wine.data"))
        features, classes = load.load_wine_dataset(filepath=filepath)
    elif dataset == 'flowers':
        filepath = os.path.join("Datasets", os.path.join("flowers", "RESNET")+".npz")
        features = np.load(filepath)["features"]
        classes = [i//50 for i in range(1360)]
    
    features_size = features.shape[1]
    
    n_elems = features.shape[0] 

    if plot:
        dict = {0: 'sepal lenght', 1: 'sepal width',
                2: 'petal length', 3: 'petal width'}
        fT = features.T
        for i in range(features_size):
            #fT[i] = sorted(fT[i])
            out = "outputs/iris/Features" + str(i+1) + "Iris.pdf"
            plots.plot_features(fT[i], dict[i], out)

    aux_matrix = np.ndarray(
        shape=(n_elems, features_size), dtype=np.object)
    features_matrix = np.ndarray(
        shape=(features_size, n_elems), dtype=np.object)

    for i in range(n_elems):
        for j in range(features_size):
            image = img.Features(
                index=i, feature=features[i, j], classification=classes[i])
            aux_matrix[i, j] = image

    features_matrix = aux_matrix.T

    # Sorting each feature
    for i in range(features_size):
        features_matrix[i] = sorted(
            features_matrix[i], key=attrgetter('feature'))

    # Saving sorted features in file
    features_lists_path = os.path.join("outputs", dataset)
    save.save_features_file(features_lists_path, features_matrix)

    return features_matrix


def evaluatePatX(data, top_k):
    n_elems = data.shape[0]
    acc = 0
    acc_list = []

    # Considering img_0 in the accuracy evaluation
    for i in range(n_elems):
        query_class = data[i, 0].classification
        acc = 0
        for j in range(top_k):
            if data[i, j].classification == query_class:
                acc += 1
        acc_list.append(acc/top_k)
 
    print("Accuracy at P@{}: {}".format(top_k, round(statistics.mean(acc_list),3)))


def generate_ranked_lists_using_features(dataset,features):
    n_features = features.shape[0]
    n_elems = features.shape[1]

    aux_features = deepcopy(features)

    n_for_each_list = 20
           
    #print("n_for_each_list:",n_for_each_list)

    #n_for_each_list = floor(top_k / n_features)        
    
    ranked_lists = np.ndarray(shape=(n_elems, n_for_each_list), dtype=np.object)

    # search neighborhood of each index, generate a KNN, return a structure
    # with ranked lists from all images with size top_k each
    for i in range(n_elems):
        knns = []
        for j in range(n_features):
            feat = features[j, i].feature
            feat_list = [features[j, k].feature for k in range(n_elems)]
            pos = binarySearch(feat_list, 0, len(feat_list)-1, feat)

            # Search for the correct element
            aux = pos
            for k in range(n_elems):
                if (aux-k > 0) and features[j, aux-k].index == i:
                    aux = aux-k
                    break
                if (aux+k < n_elems-1) and features[j, aux+k].index == i:
                    aux = aux+k
                    break

            pos = aux

            n = n_for_each_list - 1
            n_left = ceil(n/2)
            n_right = n - n_left

            if pos - n_left < 0:
                while pos - n_left < 0 or n_left > 0:
                    n_left -= 1
                    n_right += 1
            elif pos + n_right > n_elems-1:
                while pos + n_right > n_elems-1 or n_right > 0:
                    n_right -= 1
                    n_left += 1

            if n_left == -1:
                n_left = 0
                n_right -= 1
            elif n_right == -1:
                n_right = 0
                n_left -= 1

            aux_knn = []

            aux_knn = np.ndarray(
                shape=(n_for_each_list), dtype=np.object)
            
            l3 = np.ndarray(shape=(n_for_each_list-1), dtype=np.object)
            
            l1 = [aux_features[j, pos] for pos in range(pos-n_left, pos)]
            l2 = [aux_features[j, pos] for pos in range(pos+1, pos+n_right+1)]

            # Compute distance between features
            for k in range(len(l1)):
                new_value = round(abs(l1[k].get_feature() - aux_features[j,pos].get_feature()),4)
                l1[k].set_feature(new_value)
            for k in range(len(l2)):
                new_value = round(abs(l2[k].get_feature() - aux_features[j,pos].get_feature()),4)
                l2[k].set_feature(new_value)

            l1_len=0
            l2_len=0
            for k in range(len(l3)):
                if l1_len < len(l1):
                    l3[k] = l1[l1_len]
                    l1_len += 1
                elif l2_len < len(l2):
                    l3[k] = l2[l2_len]
                    l2_len += 1
        
            aux_knn[0] = features[j,pos]
            aux_knn[1:] = l3
            '''
            for k in range(n_for_each_list):
                print(aux_knn[k].feature, end=' ')
            print("\n")
            '''

            knns.append(aux_knn)

        # Knns for img_i are ready here
#       compute intersection
        '''
        for k in range(len(knns)):
            print([knns[k][i].index for i in range(top_k)])
        '''

        aux_knns = deepcopy(knns)

        aux_set = []
        final_set = []

        aux_knns[0][0].set_feature(0.0)
        final_set.append(aux_knns[0][0])

        for k in range(1,n_for_each_list):
            for f in range(n_features):
                aux_set.append(aux_knns[f][k]) 

        aux_set = sorted(aux_set, key=attrgetter('feature'))
        
        final_set[1:] = aux_set

        final_set = list(toolz.unique(final_set, key=lambda x: x.index))
    
        ranked_lists[i] = deepcopy(final_set[:n_for_each_list])

    #Saving sorted features in file
    ranked_lists_path = os.path.join("outputs", dataset)
    save.save_alternative_ranked_lists_file(ranked_lists_path, ranked_lists)

    return ranked_lists

# Returns index of x in arr if present, else -1
def binarySearch(arr, l, r, x):

    # Check base case
    if r >= l:

        mid = int(l + (r - l)/2)

        # If element is present at the middle itself
        if arr[mid] == x:
            return mid

        # If element is smaller than mid, then it can only
        # be present in left subarray
        elif arr[mid] > x:
            return binarySearch(arr, l, mid-1, x)

        # Else the element can only be present in right subarray
        else:
            return binarySearch(arr, mid+1, r, x)

    else:
        # Element is not present in the array
        return -1


def main():
    n_exec = 5
    dataset = "iris"
    if dataset == "iris":
        # IRIS DATASET
        print("Iris dataset...")

        print("Ranked lists generated using distances...")
        print("Computing nxn distances and ordering n ranked lists using nlogn algoritm -> n^2*logn")
        time_list = []
        for i in range(n_exec):
            start = time.time()
            create_ranked_lists_files("iris")
            end = time.time()
            time_list.append(end-start) 
        print("Elapsed time:",round(statistics.mean(time_list),4), "seconds")
        lists = load.load_ranked_lists_file("outputs/iris/")
        evaluatePatX(lists, top_k=20)
        
        print("Ranked lists generated using features...")
        print("Creating ordered features structure...")
        for i in range(n_exec):
            start = time.time()
            features = create_features_file("iris", plot=False)
            end = time.time()
            time_list.append(end-start) 
        print("Elapsed time:",round(statistics.mean(time_list),4), "seconds")
        #features = load.load_features_file("outputs/iris/")
        # gerar ranks com as features
        
        print("Creating ranked lists using ordered features structure...")
        for i in range(n_exec):
            start = time.time()
            generate_ranked_lists_using_features("iris",features)
            end = time.time()
            time_list.append(end-start) 
        print("Elapsed time:",round(statistics.mean(time_list),4), "seconds")


        new_lists = load.load_ranked_lists_alternative_file("outputs/iris/")
        evaluatePatX(new_lists, top_k=20)
    elif dataset == 'wine':
        print("Wine dataset...")
        print("Ranked lists generated using distances...")
        print("Computing nxn distances and ordering n ranked lists using nlogn algoritm -> n^2*logn")
        time_list = []
        for i in range(n_exec):
            start = time.time()
            create_ranked_lists_files("wine")
            end = time.time()
            time_list.append(end-start) 
        print("Elapsed time:",round(statistics.mean(time_list),4), "seconds")
        lists = load.load_ranked_lists_file("outputs/wine/")
        evaluatePatX(lists, top_k=20)
        
        print("Ranked lists generated using features...")
        print("Creating ordered features structure...")
        for i in range(n_exec):
            start = time.time()
            features = create_features_file("wine", plot=False)
            end = time.time()
            time_list.append(end-start) 
        print("Elapsed time:",round(statistics.mean(time_list),4), "seconds")
        #features = load.load_features_file("outputs/wine/")
        # gerar ranks com as features
        
        print("Creating ranked lists using ordered features structure...")
        for i in range(n_exec):
            start = time.time()
            generate_ranked_lists_using_features("wine",features)
            end = time.time()
            time_list.append(end-start) 
        print("Elapsed time:",round(statistics.mean(time_list),4), "seconds")


        new_lists = load.load_ranked_lists_alternative_file("outputs/wine/")
        evaluatePatX(new_lists, top_k=20)

    elif dataset == 'flowers':
        # FLOWERS DATASET
        print("Flowers dataset...")
        print("Ranked lists generated using distances...")
        print("Computing nxn distances and ordering n ranked lists using nlogn algoritm -> n^2*logn")
        time_list = []
        for i in range(n_exec):
            start = time.time()
            create_ranked_lists_files("flowers")
            end = time.time()
            time_list.append(end-start) 
        print("Elapsed time:",round(statistics.mean(time_list),4), "seconds")
        lists = load.load_ranked_lists_file("outputs/flowers/")
        evaluatePatX(lists, top_k=20)
        
        print("Ranked lists generated using features...")
        print("Creating ordered features structure...")
        for i in range(n_exec):
            start = time.time()
            features = create_features_file("flowers", plot=False)
            end = time.time()
            time_list.append(end-start) 
        print("Elapsed time:",round(statistics.mean(time_list),4), "seconds")
        #features = load.load_features_file("outputs/flowers/")
        # gerar ranks com as features
        
        print("Creating ranked lists using ordered features structure...")
        for i in range(n_exec):
            start = time.time()
            generate_ranked_lists_using_features("flowers",features)
            end = time.time()
            time_list.append(end-start) 
        print("Elapsed time:",round(statistics.mean(time_list),4), "seconds")


        new_lists = load.load_ranked_lists_alternative_file("outputs/flowers/")
        evaluatePatX(new_lists, top_k=20)

if __name__ == "__main__":
    main()
