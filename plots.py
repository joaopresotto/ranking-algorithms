import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
import matplotlib.patheffects as pe

plt.style.use('ggplot')

def plot_features(features, feature_name, file_path):
    f1 = plt.figure()
    plt.plot(features, label=str(feature_name), color='C0')
    plt.title("Feature: " + str(feature_name) + ".",
              fontsize=10, fontweight='bold')
    plt.xlabel("n", fontsize=10)
    plt.ylabel("feature", fontsize=10)
    plt.legend(fontsize=9)
    f1.savefig(str(file_path), format='pdf', bbox_inches='tight')
    plt.cla()