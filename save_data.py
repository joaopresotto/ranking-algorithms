import os
import numpy as np

def save_ranked_lists_file(dir_name, ranked_lists):
    try:
    # Create target Directory
        os.mkdir(dir_name)
    except FileExistsError:
        pass

    #print("Saving ranked lists in:", dir_name)

    np.save(os.path.join(dir_name,"ranked_lists"),ranked_lists)

def save_features_file(dir_name, features):
    try:
    # Create target Directory
        os.mkdir(dir_name)
    except FileExistsError:
        pass

    #print("Saving features lists in:", dir_name)

    np.save(os.path.join(dir_name,"feature_lists"),features)

def save_alternative_ranked_lists_file(dir_name, ranked_lists):
    try:
    # Create target Directory
        os.mkdir(dir_name)
    except FileExistsError:
        pass

    #print("Saving alternative ranked lists in:", dir_name)

    np.save(os.path.join(dir_name,"ranked_lists_alternative"),ranked_lists)
