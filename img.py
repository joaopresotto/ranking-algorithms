class Image(object):
    def __init__(self,index,distance,classification):
        self.index = index
        self.distance = distance
        self.classification = classification

    def get_index(self):
        return self.index
    
    def get_distance(self):
        return self.distance

    def get_classification(self):
        return self.classification

class Features(object):
    def __init__(self,index,feature,classification):
        self.index = index
        self.feature = feature
        self.classification = classification

    def set_feature(self,feature):
        self.feature = feature

    def get_index(self):
        return self.index

    def get_feature(self):
        return self.feature

    def get_classification(self):
        return self.classification