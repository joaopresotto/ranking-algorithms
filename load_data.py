import os
import numpy as np


def load_iris_dataset(filepath):
    file = open(filepath, "r")

    number_of_samples = 150
    number_of_attributes = 4

    X = np.zeros((number_of_samples, number_of_attributes))
    d = np.ndarray(shape=(number_of_samples), dtype=np.object)

    i, lines = 0, file.readlines()

    for line in lines:
        line = line.strip()  # Removendo "/n"

        sepal_lenght, sepal_width, petal_lenght, petal_width, classification = line.split(
            ",")

        X[i] = np.array([sepal_lenght, sepal_width,
                         petal_lenght, petal_width])

        '''
        if classification == "Iris-setosa":
            d[i] = 0
        elif classification == "Iris-versicolor":
            d[i] = 1
        else: # classification == "Iris-virginica"
            d[i] = 2
        '''

        d[i] = classification

        i += 1

    file.close()

    return X, d


def load_wine_dataset(filepath):
    file = open(filepath, "r")
    
    number_of_samples, number_of_attributes = 178, 13

    X = np.zeros((number_of_samples,number_of_attributes))
    d = np.zeros((number_of_samples))
    
    i, lines = 0, file.readlines()
    
    for line in lines:
        line = line.strip() # removendo "\n"
        
        # pegando atributos e classificação
        classification, alcohol, malic_acid, ash, ash_alcalinity, magnesium, phenols, flavanoids, non_flav_phenols, protoan, color_int, hue, od, proline = line.split(",")
        
        X[i] = np.array([alcohol, malic_acid, ash, ash_alcalinity, magnesium, phenols, flavanoids, non_flav_phenols, protoan, color_int, hue, od, proline])
        
        d[i] = classification
        
        i+=1
        
    file.close()
    
    return X, d 

def load_ranked_lists_file(path):
    ranked_lists = np.load(os.path.join(path,"ranked_lists.npy"))
    return ranked_lists

def load_features_file(path):
    features = np.load(os.path.join(path,"feature_lists.npy"))
    return features

def load_ranked_lists_alternative_file(path):
    ranked_lists_alternative = np.load(os.path.join(path,"ranked_lists_alternative.npy"))
    return ranked_lists_alternative
